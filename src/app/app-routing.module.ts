import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { ExaminerComponent } from './page/examiner/examiner.component';
import { AdminComponent } from './page/admin/admin.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'giam-thi-1', component: AdminComponent, data: {examiner: 1}},
  {path: 'giam-thi-2', component: ExaminerComponent, data: {examiner: 2}},
  {path: 'giam-thi-3', component: ExaminerComponent, data: {examiner: 3}},
  {path: 'giam-thi-4', component: ExaminerComponent, data: {examiner: 4}},
  {path: 'giam-thi-5', component: ExaminerComponent, data: {examiner: 5}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
