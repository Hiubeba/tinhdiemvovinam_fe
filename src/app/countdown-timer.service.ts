import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountdownTimerService {

  private countdown$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private currentTime: number = 0;
  private countdownTimerRef: any;
  private isRunning: boolean = false;

  constructor() { }

  getCountdownObservable(): Observable<number> {
    return this.countdown$.asObservable();
  }

  initializeTimer(seconds: number): void {
    this.currentTime = seconds;
    this.countdown$.next(this.currentTime);
  }

  startCountdown(): void {
    if (!this.isRunning) {
      this.isRunning = true;
      this.runTimer();
    }
  }

  private runTimer(): void {
    this.countdownTimerRef = setInterval(() => {
      if (this.currentTime > 0) {
        this.currentTime--;
        this.countdown$.next(this.currentTime);
      } else {
        this.stopCountdown();
      }
    }, 1000);
  }

  stopCountdown(): void {
    if (this.countdownTimerRef) {
      clearInterval(this.countdownTimerRef);
      this.countdownTimerRef = null;
    }
    this.isRunning = false;
  }

  resetCountdown(): void {
    this.stopCountdown();
    this.currentTime = 0;
    this.countdown$.next(this.currentTime);
  }

  setTime(seconds: number): void {
    this.stopCountdown();
    this.currentTime = seconds;
    this.countdown$.next(this.currentTime);
  }
}