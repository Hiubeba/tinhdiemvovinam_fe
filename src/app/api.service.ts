import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { env } from '../../env';
interface Scores {
  red: number;
  blue: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiDataService {
  api: string = env.apidev;

  constructor(private http: HttpClient) {}

  private _refreshNeeded$ = new Subject<void>();

  get refreshNeeded$() {
    return this._refreshNeeded$;
  }

  getDataMark(): Observable<Scores> {
    return this.http.get<Scores>(`${this.api}/scores`);
  }

  postDataMark(body: any): Observable<any> {
    return this.http.post(`${this.api}/vote`, body).pipe(
      tap(() => {
        this._refreshNeeded$.next();
      })
    );
  }

  postResetMark(body: any): Observable<any> {
    return this.http.post(`${this.api}/reset`, body).pipe(
      tap(() => {
        this._refreshNeeded$.next();
      })
    );
  }
}
