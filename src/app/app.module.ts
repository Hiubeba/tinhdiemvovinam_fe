import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WatchComponent } from './components/watch/watch.component';
import { MarkComponent } from './components/mark/mark.component';
import { HomeComponent } from './page/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ExaminerComponent } from './page/examiner/examiner.component';
import { ApiDataService } from './api.service';
import { ButtonWatchComponent } from './components/button-watch/button-watch.component';
import { AdminComponent } from './page/admin/admin.component';
import { InputNameComponent } from './components/input-name/input-name.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WatchComponent,
    MarkComponent,
    HomeComponent,
    ExaminerComponent,
    ButtonWatchComponent,
    AdminComponent,
    InputNameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ApiDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
