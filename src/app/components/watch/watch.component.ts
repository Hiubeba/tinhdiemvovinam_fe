import { Component, OnInit } from '@angular/core';
import { Observable, map } from 'rxjs';
import { CountdownTimerService } from '../../countdown-timer.service';
import { env } from '../../../../env';
const time = env.time
@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.css']
})
export class WatchComponent implements OnInit {
  formattedTime$!: Observable<string>;
  isRunning: boolean = false;

  constructor(private countdownTimerService: CountdownTimerService) { }

  ngOnInit(): void {
    this.countdownTimerService.initializeTimer(time); // Khởi tạo với 5 phút
    this.formattedTime$ = this.countdownTimerService.getCountdownObservable().pipe(
      map(seconds => this.formatTime(seconds))
    );
  }

  private formatTime(seconds: number): string {
    const minutes: number = Math.floor(seconds / 60);
    const remainingSeconds: number = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
  }
}