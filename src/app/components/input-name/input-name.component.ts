import { Component } from '@angular/core';

@Component({
  selector: 'app-input-name',
  templateUrl: './input-name.component.html',
  styleUrl: './input-name.component.css'
})
export class InputNameComponent {
  inputValue: string = '';
  savedValue: string = 'Click to edit';
  isInputVisible: boolean = false;

  saveValue() {
    if (this.inputValue.trim()) {
      this.savedValue = this.inputValue.trim();
      this.inputValue = '';
      this.isInputVisible = false;
    }
  }

  showInput() {
    this.inputValue = this.savedValue;
    this.isInputVisible = true;
  }

  hideInput() {
    if (!this.inputValue.trim()) {
      this.isInputVisible = false;
    }
  }
}
