import { Component, OnInit } from '@angular/core';
import { CountdownTimerService } from '../../countdown-timer.service';
import { env } from '../../../../env';
const time = env.time
@Component({
  selector: 'app-button-watch',
  templateUrl: './button-watch.component.html',
  styleUrl: './button-watch.component.css'
})
export class ButtonWatchComponent implements OnInit{
  isRunning: boolean = false;

  constructor(private countdownTimerService: CountdownTimerService){}

  ngOnInit(): void {
    
  }

  startTimer(): void {
    this.countdownTimerService.startCountdown();
    this.isRunning = true;
  }

  stopTimer(): void {
    this.countdownTimerService.stopCountdown();
    this.isRunning = false;
  }

  resetTimer(): void {
    this.countdownTimerService.resetCountdown();
    this.countdownTimerService.initializeTimer(time); // Reset về 5 phút
    this.isRunning = false;
  }
}
