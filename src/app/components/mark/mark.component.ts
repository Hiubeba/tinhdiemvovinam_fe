import { Component, OnInit } from '@angular/core';
import { ApiDataService } from '../../api.service';
import { io } from 'socket.io-client';
import { env } from '../../../../env';

@Component({
  selector: 'app-mark',
  templateUrl: './mark.component.html',
  styleUrls: ['./mark.component.css']
})
export class MarkComponent implements OnInit {
  markRed: number = 0;
  markBlue: number = 0;
  private socket: any;

  constructor(private getApi: ApiDataService) {
    // Kết nối đến server Socket.IO
    this.socket = io(env.apidev);
  }

  ngOnInit(): void {
    this.getMark();

    // Xử lý sự kiện khi nhận được tin nhắn từ Socket.IO
    this.socket.on('scoreUpdate', (data: any) => {
      if (data.team === 'red') {
        this.markRed = data.score;
      } else if (data.team === 'blue') {
        this.markBlue = data.score;
      } else {
        this.markRed = data.red;
        this.markBlue = data.blue;
      }
    });

    this.getApi.refreshNeeded$.subscribe(() => {
      this.getMark();
    });
  }

  // Hàm lấy điểm số từ API
  getMark() {
    this.getApi.getDataMark().subscribe((res: any) => {
      this.markRed = res.red;
      this.markBlue = res.blue;
    });
  }
}
