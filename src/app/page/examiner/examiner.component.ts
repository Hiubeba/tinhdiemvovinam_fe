import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiDataService } from '../../api.service';
import { io } from 'socket.io-client';
import { env } from '../../../../env';

interface ScoreUpdate {
  team: 'red' | 'blue';
  score: number;
}

interface Scores {
  red: number;
  blue: number;
}

@Component({
  selector: 'app-examiner',
  templateUrl: './examiner.component.html',
  styleUrls: ['./examiner.component.css']
})
export class ExaminerComponent implements OnInit {
  examiner: number = 0;
  scores: Scores = { red: 0, blue: 0 };
  private socket: any;

  constructor(private route: ActivatedRoute, private getApi: ApiDataService) {
    this.socket = io(env.apidev); // Kết nối tới server socket.io
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.examiner = data['examiner'];
    });

    this.socket.on('scoreUpdate', (data: ScoreUpdate) => {
      this.scores[data.team] = data.score;
    });

    this.getScores(); // Lấy điểm số ban đầu
  }

  markRed(number: number) {
    const body = {
      judgeId: this.examiner,
      team: 'red',
      vote: number
    };
    this.getApi.postDataMark(body).subscribe();
  }

  markBlue(number: number) {
    const body = {
      judgeId: this.examiner,
      team: 'blue',
      vote: number
    };
    this.getApi.postDataMark(body).subscribe();
  }

  private getScores() {
    this.getApi.getDataMark().subscribe((scores: Scores) => {
      this.scores = scores;
    });
  }
}
