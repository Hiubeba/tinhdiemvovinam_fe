import { Component } from '@angular/core';
import { ApiDataService } from '../../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  constructor(private getApi: ApiDataService){}
  reset(){
    const body={}
    this.getApi.postResetMark(body).subscribe((res: any)=>{
      
    })
  }
}
